package commands

import (
	"log"
	"os"
	"strconv"

	"github.com/spf13/cobra"
	nrDashboard "gitlab.com/potyl/newrelic-dashboard/internal/newrelic"
)

func init() {
	rootCmd.AddCommand(createCommand())
}

func createCommand() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "create account-id file",
		Short: "Create a dashboard",
		Long: longDoc(`
			Creates a newrelic dashboard into the given account id.
			The dashboard is expected to be in json format.
			If the filename is '-' then the dashboard is read from stdin.
		`),
		Args: cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			createCmdRun(cmd, args)
		},
	}

	return cmd
}

func createCmdRun(cmd *cobra.Command, args []string) {
	accountId, err := strconv.Atoi(args[0])
	if err != nil {
		log.Fatal("Can't parse account id: ", err)
	}
	accountIdCorrector := func(gotAccountId int) int {
		if gotAccountId == 0 {
			return accountId
		}
		return gotAccountId
	}
	fileName := args[1]
	dashboard := parseJsonDashboard(fileName, accountIdCorrector)

	client := nrDashboard.NewRelicClient()
	response, err := nrDashboard.CreateDashboard(client, accountId, dashboard)
	if err != nil {
		log.Fatal("Failed to create the dashboard: ", err)
	}

	printJson(os.Stdout, response)

	reportGraphQLErrors(response.DashboardCreate.Errors)
}
