package commands

import (
	"testing"
)

func TestLongDoc(t *testing.T) {
	tests := []struct {
		value    string
		expected string
	}{
		{"", ""},
		{"Hello World", "Hello World"},
		{"\n", ""},
		{"   Hello\n   World", "Hello\nWorld"},
		{"   Hello\n   World\n", "Hello\nWorld\n"},
		{"\n   Hello\n   World", "Hello\nWorld"},
		{"\n   Hello\n   World\n", "Hello\nWorld\n"},
	}

	for _, test := range tests {
		got := longDoc(test.value)
		if got != test.expected {
			t.Errorf("longDoc() test failed, got: %v, expected: %v.", got, test.expected)
		}
	}
}
