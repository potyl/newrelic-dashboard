package commands

import (
	_ "embed"
	"testing"

	"gitlab.com/potyl/newrelic-dashboard/internal/newrelic"
	"gitlab.com/potyl/newrelic-dashboard/internal/utilstest"
)

//go:embed fixtures/viz-table.json
var jsonDashboardTable []byte

func TestParseDashboardTable(t *testing.T) {
	got := parseJsonDashboardBytes(jsonDashboardTable, utilstest.AccountIdCorrector)

	expected := newrelic.JsonDashboard{
		Name:        "Test",
		Permissions: "PUBLIC_READ_WRITE",
		Pages: []newrelic.JsonPage{
			{
				Name: "My Tables",
				Widgets: []newrelic.JsonWidget{
					{
						Title: "Transactions Overview",
						Layout: newrelic.JsonLayout{
							Column: 6,
							Row:    2,
							Width:  7,
							Height: 3,
						},
						Visualization: newrelic.JsonVisualization{
							Id: "viz.table",
						},
						RawConfiguration: newrelic.JsonRawConfiguration{
							NrqlQueries: []newrelic.JsonNrqlQuery{
								{
									Query: "SELECT count(*) FROM Transactions",
								},
							},
						},
					},

					{
						Title: "Errors Overview",
						Layout: newrelic.JsonLayout{
							Column: 6,
							Row:    5,
							Width:  7,
							Height: 3,
						},
						Visualization: newrelic.JsonVisualization{
							Id: "viz.table",
						},
						RawConfiguration: newrelic.JsonRawConfiguration{
							NrqlQueries: []newrelic.JsonNrqlQuery{
								{
									AccountId: 2345678,
									Query:     "SELECT count(*) FROM Errors",
								},
							},
						},
					},
				},
			},
		},
	}

	utilstest.Compare(t, "parseDashboard(table)", got, expected)
}

func TestTerraformDashboardTable(t *testing.T) {
	dashboard := parseJsonDashboardBytes(jsonDashboardTable, utilstest.AccountIdCorrector)
	got := newrelic.ToTerraform(dashboard, "table")

	expected := utilstest.TerraformContent(`
		resource "newrelic_one_dashboard" "table" {
			name        = "Test"
			permissions = "public_read_write"

			page {
				name = "My Tables"

				widget_table {
					title = "Transactions Overview"

					row    = 2
					column = 6
					width  = 7
					height = 3

					nrql_query {
						query = "SELECT count(*) FROM Transactions"
					}
				}

				widget_table {
					title = "Errors Overview"

					row    = 5
					column = 6
					width  = 7
					height = 3

					nrql_query {
						query      = "SELECT count(*) FROM Errors"
						account_id = 2345678
					}
				}
			}
		}
	`)

	utilstest.Compare(t, "terraform(table)", got, expected)
}
