package commands

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

// Version of the application
var version = "devel"

var rootCmd = &cobra.Command{
	Use:   "newrelic-dashboard",
	Short: "Convert a newrelic dashboard to terraform",
	Long:  `Convert a newrelic dashboard export into terraform's format`,
	RunE:  nil,
}

func Execute(appVersion string) {
	version = appVersion
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
