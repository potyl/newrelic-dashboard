package commands

import (
	"log"

	"github.com/spf13/cobra"
	nrDashboard "gitlab.com/potyl/newrelic-dashboard/internal/newrelic"
)

func init() {
	rootCmd.AddCommand(updateCommand())
}

type updateArgsType struct {
	accountId int
}

func updateCommand() *cobra.Command {
	var flags = updateArgsType{
		accountId: 0,
	}

	var cmd = &cobra.Command{
		Use:   "update guid file",
		Short: "Update a dashboard",
		Long: longDoc(`
			Updates the newrelic dashboard corresponding to the given guid.
			The dashboard is expected to be in json format.
			If the filename is '-' then the dashboard is read from stdin.
		`),
		Args: cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			updateCmdRun(cmd, args, flags)
		},
	}

	cmd.Flags().IntVarP(&flags.accountId, "account-id", "a", flags.accountId, "the account id")

	return cmd
}

func updateCmdRun(cmd *cobra.Command, args []string, flags updateArgsType) {
	accountIdCorrector := nrDashboard.AccountIdCorrector(flags.accountId)
	fileName := args[1]
	dashboard := parseJsonDashboard(fileName, accountIdCorrector)

	guid := args[0]
	client := nrDashboard.NewRelicClient()
	response, err := nrDashboard.UploadDashboard(client, guid, dashboard)
	if err != nil {
		log.Fatal("Failed to update the dashboard: ", err)
	}

	reportGraphQLErrors(response.DashboardUpdate.Errors)
}
