package commands

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"gitlab.com/potyl/newrelic-dashboard/internal/newrelic"
)

type JsonDashboard = newrelic.JsonDashboard

func printJson(writer io.Writer, data any) {
	bytes, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		log.Fatal("Failed to serialize to json ", err)
	}
	writer.Write(bytes)
	fmt.Fprintln(writer)
}

func longDoc(doc string) string {
	doc = strings.TrimLeft(doc, "\r\n")
	return newrelic.FixIndentation(doc, "")
}

func openRead(fileName string) io.ReadCloser {
	if fileName == "-" {
		return io.NopCloser(os.Stdin)
	}

	handle, err := os.Open(fileName)
	if err != nil {
		log.Fatal("Failed to open file ", fileName, " in read mode : ", err)
	}

	return handle
}

func openWrite(fileName string) io.WriteCloser {
	if fileName == "-" {
		return newNoopWriteCloser(os.Stdout)
	}

	handle, err := os.Create(fileName)
	if err != nil {
		log.Fatal("Failed to open file ", fileName, " in write mode : ", err)
	}

	return handle
}

func parseJsonDashboard(fileName string, accountIdCorrector newrelic.AccountIdCorrectorType) JsonDashboard {
	jsonFile := openRead(fileName)
	defer jsonFile.Close()

	bytes, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		log.Fatal("Failed to read dashboard: ", err)
	}

	dashboard := parseJsonDashboardBytes(bytes, accountIdCorrector)
	return dashboard
}

func parseJsonDashboardBytes(bytes []byte, accountIdCorrector newrelic.AccountIdCorrectorType) JsonDashboard {
	var dashboard JsonDashboard
	if err := json.Unmarshal(bytes, &dashboard); err != nil {
		log.Fatal("Failed to parse dashboard: ", err)
	}

	newrelic.CleanDashboardAccountId(&dashboard, accountIdCorrector)

	return dashboard
}

func reportGraphQLErrors(errors []newrelic.GraphQLError) {
	if len(errors) == 0 {
		return
	}

	fmt.Printf("Got %v errors:\n", len(errors))
	for _, error := range errors {
		fmt.Println("  ", error.Description)
	}
	os.Exit(1)
}

func newNoopWriteCloser(writer io.Writer) io.WriteCloser {
	return noopWriteCloser{writer}
}

type noopWriteCloser struct {
	io.Writer
}

func (noopWriteCloser) Close() error {
	return nil
}
