package commands

import (
	"log"

	"github.com/spf13/cobra"
	nrDashboard "gitlab.com/potyl/newrelic-dashboard/internal/newrelic"
)

func init() {
	rootCmd.AddCommand(deleteCommand())
}

func deleteCommand() *cobra.Command {
	return &cobra.Command{
		Use:   "delete guid",
		Short: "Delete a dashboard",
		Long: longDoc(`
			Deletes the newrelic dashboard corresponding to the given guid.
		`),
		Args: cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			deleteCmdRun(cmd, args)
		},
	}
}

func deleteCmdRun(cmd *cobra.Command, args []string) {
	guid := args[0]
	client := nrDashboard.NewRelicClient()
	response, err := nrDashboard.DeleteDashboard(client, guid)
	if err != nil {
		log.Fatal("Failed to delete the dashboard: ", err)
	}

	reportGraphQLErrors(response.DashboardDelete.Errors)
}
