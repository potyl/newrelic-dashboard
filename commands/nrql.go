package commands

import (
	"log"
	"os"
	"strconv"

	"github.com/spf13/cobra"
	nrDashboard "gitlab.com/potyl/newrelic-dashboard/internal/newrelic"
)

func init() {
	rootCmd.AddCommand(nrqlCommand())
}

func nrqlCommand() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "nrql account-id NRQL",
		Short: "Run an NRQL query",
		Long: longDoc(`
			Runs a NRQL query for the given account id.
			The results will be output to the in json format.
		`),
		Args: cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			createCmdNrql(cmd, args)
		},
	}

	return cmd
}

func createCmdNrql(cmd *cobra.Command, args []string) {
	accountId, err := strconv.Atoi(args[0])
	if err != nil {
		log.Fatal("Can't parse account id: ", err)
	}
	nrql := args[1]

	client := nrDashboard.NewRelicClient()

	response, err := nrDashboard.ExecuteNrql(client, accountId, nrql)
	if err != nil {
		log.Fatal("Failed to execute NRQL: ", err)
	}

	printJson(os.Stdout, response.Actor.Account.Nrql.Results)
}
