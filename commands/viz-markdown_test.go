package commands

import (
	_ "embed"
	"testing"

	"gitlab.com/potyl/newrelic-dashboard/internal/newrelic"
	"gitlab.com/potyl/newrelic-dashboard/internal/utilstest"
)

//go:embed fixtures/viz-markdown.json
var jsonDashboardMarkdown []byte

func TestParseDashboardMarkdown(t *testing.T) {
	got := parseJsonDashboardBytes(jsonDashboardMarkdown, utilstest.AccountIdCorrector)

	expected := newrelic.JsonDashboard{
		Name:        "Test",
		Permissions: "PUBLIC_READ_WRITE",
		Pages: []newrelic.JsonPage{
			{
				Name: "My Markdown",
				Widgets: []newrelic.JsonWidget{
					{
						Layout: newrelic.JsonLayout{
							Column: 1,
							Row:    1,
							Width:  12,
							Height: 1,
						},
						Visualization: newrelic.JsonVisualization{
							Id: "viz.markdown",
						},
						RawConfiguration: newrelic.JsonRawConfiguration{
							Text: "## This is some markdown",
						},
					},
				},
			},
		},
	}

	utilstest.Compare(t, "parseDashboard(markdown)", got, expected)
}

func TestTerraformDashboardMarkdown(t *testing.T) {
	dashboard := parseJsonDashboardBytes(jsonDashboardMarkdown, utilstest.AccountIdCorrector)
	got := newrelic.ToTerraform(dashboard, "markdown")

	expected := utilstest.TerraformContent(`
		resource "newrelic_one_dashboard" "markdown" {
			name        = "Test"
			permissions = "public_read_write"

			page {
				name = "My Markdown"

				widget_markdown {
					title = ""

					row    = 1
					column = 1
					width  = 12
					height = 1

					text = "## This is some markdown"
				}
			}
		}
	`)

	utilstest.Compare(t, "terraform(markdown)", got, expected)
}
