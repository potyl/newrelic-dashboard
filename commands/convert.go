package commands

import (
	"fmt"

	"github.com/spf13/cobra"
	nrDashboard "gitlab.com/potyl/newrelic-dashboard/internal/newrelic"
)

func init() {
	rootCmd.AddCommand(convertCommand())
}

type convertArgsType struct {
	accountId int
	name      string
}

func convertCommand() *cobra.Command {
	var flags = convertArgsType{
		accountId: 0,
		name:      "default",
	}

	var cmd = &cobra.Command{
		Use:   "convert file",
		Short: "Convert a dashboard",
		Long: longDoc(`
			Convert a json dashboard to the terraform format.
			If the filename is '-' then the dashboard is read from stdin.
		`),
		Args: cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			convertCmdRun(cmd, args, flags)
		},
	}

	cmd.Flags().IntVarP(&flags.accountId, "account-id", "a", flags.accountId, "the account id")
	cmd.Flags().StringVarP(&flags.name, "name", "n", flags.name, "the terraform resource name")

	return cmd
}

func convertCmdRun(cmd *cobra.Command, args []string, flags convertArgsType) {
	accountIdCorrector := nrDashboard.AccountIdCorrector(flags.accountId)
	fileName := args[0]
	dashboard := parseJsonDashboard(fileName, accountIdCorrector)
	output := nrDashboard.ToTerraform(dashboard, flags.name)
	fmt.Print(output)
}
