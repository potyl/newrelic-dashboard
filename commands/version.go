package commands

import (
	"fmt"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(versionCommand())
}

func versionCommand() *cobra.Command {
	return &cobra.Command{
		Use:   "version",
		Short: "Prints the version",
		Long:  `Prints the version`,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("version: ", version)
		},
	}
}
