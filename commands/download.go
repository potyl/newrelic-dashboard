package commands

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	nrDashboard "gitlab.com/potyl/newrelic-dashboard/internal/newrelic"
)

func init() {
	rootCmd.AddCommand(downloadCommand())
}

type downloadFlagsType struct {
	accountId  int
	jsonExport bool
	rawExport  bool
	name       string
}

func downloadCommand() *cobra.Command {
	var flags = downloadFlagsType{
		accountId:  0,
		jsonExport: false,
		rawExport:  false,
		name:       "default",
	}

	var cmd = &cobra.Command{
		Use:   "download guid file",
		Short: "Download a dashboard",
		Long: longDoc(`
			Downloads a dashboard based on the given guid.
			If no format is specified then the output will be in the format of terraform.
			If the filename is '-' then the file is printed to stdout.
		`),
		Args: cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			downloadCmdRun(cmd, args, flags)
		},
	}

	cmd.Flags().IntVarP(&flags.accountId, "account-id", "a", flags.accountId, "the account id")
	cmd.Flags().BoolVarP(&flags.jsonExport, "json", "j", flags.jsonExport, "download a dashboard in json format")
	cmd.Flags().BoolVarP(&flags.rawExport, "raw", "r", flags.jsonExport, "download a dashboard in raw json format")
	cmd.Flags().StringVarP(&flags.name, "name", "n", flags.name, "the terraform resource name")

	return cmd
}

func downloadCmdRun(cmd *cobra.Command, args []string, flags downloadFlagsType) {
	client := nrDashboard.NewRelicClient()

	guid := args[0]
	fileName := args[1]
	handle := openWrite(fileName)
	defer handle.Close()

	if flags.rawExport {
		dashboard, err := nrDashboard.ExportDashboardRaw(client, guid)
		if err != nil {
			log.Fatal("Failed to export raw dashboard:", err)
		}
		printJson(handle, dashboard)
		return
	}

	dashboard, err := nrDashboard.ExportDashboard(client, guid)
	if err != nil {
		log.Fatal("Failed to export dashboard:", err)
	}

	accountIdCorrector := nrDashboard.AccountIdCorrector(flags.accountId)
	nrDashboard.CleanDashboardAccountId(&dashboard, accountIdCorrector)

	// Check if we print as json or as terraform
	var output string
	if flags.jsonExport {
		output = nrDashboard.ToJson(dashboard)
	} else {
		output = nrDashboard.ToTerraform(dashboard, flags.name)
	}

	fmt.Fprint(handle, output)
}
