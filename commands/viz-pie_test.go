package commands

import (
	_ "embed"
	"testing"

	"gitlab.com/potyl/newrelic-dashboard/internal/newrelic"
	"gitlab.com/potyl/newrelic-dashboard/internal/utilstest"
)

//go:embed fixtures/viz-pie.json
var jsonDashboardPie []byte

func TestParseDashboardPie(t *testing.T) {
	got := parseJsonDashboardBytes(jsonDashboardPie, utilstest.AccountIdCorrector)

	expected := newrelic.JsonDashboard{
		Name:        "Test",
		Permissions: "PUBLIC_READ_WRITE",
		Pages: []newrelic.JsonPage{
			{
				Name: "My Pies",
				Widgets: []newrelic.JsonWidget{
					{
						Title: "Transactions Overview",
						Layout: newrelic.JsonLayout{
							Column: 6,
							Row:    2,
							Width:  7,
							Height: 3,
						},
						Visualization: newrelic.JsonVisualization{
							Id: "viz.pie",
						},
						LinkedEntityGuids: []newrelic.JsonLinkedEntityGuid{
							{
								Guid: "FooBar",
							},
						},
						FilterCurrentDashboard: true,
						RawConfiguration: newrelic.JsonRawConfiguration{
							NrqlQueries: []newrelic.JsonNrqlQuery{
								{
									Query: "SELECT uniqueCount(session) FROM PageView FACET appName SINCE 1 day ago",
								},
							},
						},
					},

					{
						Title: "Errors Overview",
						Layout: newrelic.JsonLayout{
							Column: 6,
							Row:    5,
							Width:  7,
							Height: 3,
						},
						Visualization: newrelic.JsonVisualization{
							Id: "viz.pie",
						},
						LinkedEntityGuids: []newrelic.JsonLinkedEntityGuid{
							{
								Guid: "BarFoo",
							},
						},
						RawConfiguration: newrelic.JsonRawConfiguration{
							NrqlQueries: []newrelic.JsonNrqlQuery{
								{
									AccountId: 2345678,
									Query:     "SELECT count(*) FROM Errors FACET appName",
								},
							},
						},
					},
				},
			},
		},
	}

	utilstest.Compare(t, "parseDashboard(pie)", got, expected)
}

func TestTerraformDashboardPie(t *testing.T) {
	dashboard := parseJsonDashboardBytes(jsonDashboardMarkdown, utilstest.AccountIdCorrector)
	got := newrelic.ToTerraform(dashboard, "pie")

	expected := utilstest.TerraformContent(`
		resource "newrelic_one_dashboard" "pie" {
			name        = "Test"
			permissions = "public_read_write"

			page {
				name = "My Markdown"

				widget_markdown {
					title = ""

					row    = 1
					column = 1
					width  = 12
					height = 1

					text = "## This is some markdown"
				}
			}
		}
	`)

	utilstest.Compare(t, "terraform(markdown)", got, expected)
}
