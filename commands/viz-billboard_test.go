package commands

import (
	_ "embed"
	"testing"

	"gitlab.com/potyl/newrelic-dashboard/internal/newrelic"
	"gitlab.com/potyl/newrelic-dashboard/internal/utilstest"
)

//go:embed fixtures/viz-billboard.json
var jsonDashboardBillboard []byte

func TestParseDashboardBillboard(t *testing.T) {
	got := parseJsonDashboardBytes(jsonDashboardBillboard, utilstest.AccountIdCorrector)

	expected := newrelic.JsonDashboard{
		Name:        "Test",
		Permissions: "PUBLIC_READ_WRITE",
		Pages: []newrelic.JsonPage{
			{
				Name: "My Billboards",
				Widgets: []newrelic.JsonWidget{
					{
						Title: "Transactions Overview",
						Layout: newrelic.JsonLayout{
							Column: 6,
							Row:    2,
							Width:  7,
							Height: 3,
						},
						Visualization: newrelic.JsonVisualization{
							Id: "viz.billboard",
						},
						RawConfiguration: newrelic.JsonRawConfiguration{
							NrqlQueries: []newrelic.JsonNrqlQuery{
								{
									Query: "SELECT count(*) FROM Transactions",
								},
							},
							Thresholds: []newrelic.JsonThreshold{
								{
									AlertSeverity: "WARNING",
									Value:         0.5,
								},
								{
									AlertSeverity: "CRITICAL",
									Value:         3.5,
								},
							},
						},
					},

					{
						Title: "Errors Overview",
						Layout: newrelic.JsonLayout{
							Column: 6,
							Row:    5,
							Width:  7,
							Height: 3,
						},
						Visualization: newrelic.JsonVisualization{
							Id: "viz.billboard",
						},
						RawConfiguration: newrelic.JsonRawConfiguration{
							NrqlQueries: []newrelic.JsonNrqlQuery{
								{
									AccountId: 2345678,
									Query:     "SELECT count(*) FROM Errors",
								},
							},
						},
					}},
			},
		},
	}

	utilstest.Compare(t, "parseDashboard(billboard)", got, expected)
}

func TestTerraformDashboardBillboard(t *testing.T) {
	dashboard := parseJsonDashboardBytes(jsonDashboardBillboard, utilstest.AccountIdCorrector)
	got := newrelic.ToTerraform(dashboard, "billboard")

	expected := utilstest.TerraformContent(`
		resource "newrelic_one_dashboard" "billboard" {
			name        = "Test"
			permissions = "public_read_write"

			page {
				name = "My Billboards"

				widget_billboard {
					title = "Transactions Overview"

					row    = 2
					column = 6
					width  = 7
					height = 3

					warning  = 0.500000
					critical = 3.500000

					nrql_query {
						query = "SELECT count(*) FROM Transactions"
					}
				}

				widget_billboard {
					title = "Errors Overview"

					row    = 5
					column = 6
					width  = 7
					height = 3

					nrql_query {
						query      = "SELECT count(*) FROM Errors"
						account_id = 2345678
					}
				}
			}
		}
	`)

	utilstest.Compare(t, "terraform(billboard)", got, expected)
}
