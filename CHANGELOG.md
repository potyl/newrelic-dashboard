# Changelog

v0.0.3

- fix export for dashboards with linked entity guids
- add the command `nrql` that can be used for running arbitrary NRQL statements

v0.0.2

- billboard widget's critical and warning are now floats and properly handled in the json payload
- bullet widget's limit is now a float
- remove unused json properties
- add --raw to the download command

v0.0.1

- First release
