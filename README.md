# New Relic dashboards CLI utility

This program provides a simple command line utility that can be used for manipulating
[New Relic dashboards](https://docs.newrelic.com/docs/query-your-data/explore-query-data/dashboards/introduction-dashboards/) (listing, creating, updating).

One particularity that it offers is that it can generate a
[Terraform file](https://registry.terraform.io/providers/newrelic/newrelic/latest/docs/resources/one_dashboard) from a dashboard definition.

The conversion can be done buy pulling a dashboard definition on the fly from New Relic
or by passing a json export from a dashboard.

## Sample output

Here's a sample of the generated output:

```terraform
resource "newrelic_one_dashboard" "example_dashboard" {
  name        = "New Relic Terraform Example"
  permissions = "public_read_write"

  page {
    name = "New Relic Terraform Example"

    widget_billboard {
      title = "Requests per minute"

      row    = 1
      column = 1
      width  = 4
      height = 5

      nrql_query {
        query      = "SELECT rate(count(*), 1 minute) FROM Transaction"
        account_id = 1234567
      }
    }

    widget_bar {
      title = "Average transaction duration, by application"

      row    = 1
      column = 5
      width  = 4
      height = 5

      nrql_query {
        query      = <<-NRQL
          SELECT average(duration)
          FROM Transaction
          FACET appName
        NRQL
        account_id = 1234567
      }
    }

    widget_markdown {
      title = "Dashboard Note"

      row    = 1
      column = 9
      width  = 4
      height = 5

      text = <<-MARKDOWN
        ### Helpful Links
        
        * [New Relic One](https://one.newrelic.com)
        * [Developer Portal](https://developer.newrelic.com)
      MARKDOWN
    }
  }
}
```

The terraform output is automatically formatted.
Text fields that contain multiline texts (NRQL and markdown) use [heredoc](https://en.wikipedia.org/wiki/Here_document).

## Setup

This is is a command line program that connects to New Relic through its GraphQL API ([NerdGraph](https://docs.newrelic.com/docs/apis/nerdgraph/get-started/introduction-new-relic-nerdgraph/)).

You will need to generate a [New Relic API key](https://docs.newrelic.com/docs/apis/intro-apis/new-relic-api-keys/#overview-keys), to be more precise a **user key**, used for querying and configuration.

These keys are prefixed with `NRAK-` and have the pattern:

```key
NRAK-***************************
```

Set the key as the environment variable:

```bash
export NEW_RELIC_API_KEY='NRAK-***************************'
```

## Usage

Invoke the program with a command, refer to the `--help` message for more details:

```out
Convert a newrelic dashboard export into terraform's format

Usage:
  newrelic-dashboard [command]

Available Commands:
  completion  Generate the autocompletion script for the specified shell
  convert     Convert a dashboard
  create      Create a dashboard
  delete      Delete a dashboard
  download    Download a dashboard
  help        Help about any command
  list        Prints the dashboards
  update      Update a dashboard
  version     Prints the version

Flags:
  -h, --help   help for newrelic-dashboard

Use "newrelic-dashboard [command] --help" for more information about a command.
```

Here's a run down of some of the subcommands.

### Convert a dashboard from json to terraform

To convert a dashboard in json format to the terraform format simply do:

```bash
newrelic-dashboard convert backend-errors.json --name "backend_errors" > backend-errors.tf
```

### List dashboards

To list the dashboards available:

```bash
newrelic-dashboard list
```

### Download a dashboard

To download a dashboard from New Relic simply provide its guid and the filename to save it as (e.g. `export.tf`):

```bash
newrelic-dashboard download MTYwNjg2MnxBUE18QVBQTElDQVRJT058NDMxOTIwNTg export.tf
```

By default the dashboard is exported in the terraform format.
To export it as json use the flag `--json`.

### Update a dashboard

It is possible to update an exiting dashboard with:

```bash
newrelic-dashboard update MTYwNjg2MnxBUE18QVBQTElDQVRJT058NDMxOTIwNTg dashboard.json
```

## References

Related links to other documentation sources:

- [Terraform Provider for New Relic](https://github.com/newrelic/terraform-provider-newrelic/blob/3fdddd418668180256b57ffa6ee4a899832f75fe/newrelic/resource_newrelic_one_dashboard.go#L71)
- [New Relic - Chart types](https://docs.newrelic.com/docs/query-your-data/explore-query-data/use-charts/chart-types/)
- [NerdGraph tutorial: Move dashboards to other accounts](https://docs.newrelic.com/docs/apis/nerdgraph/examples/export-import-dashboards-using-api/)
- [Add your custom visualization to a dashboard with NerdGraph](<https://developer.newrelic.com/explore-docs/custom-viz/add-to-dashboard-nerdgraph/>)
- [GraphQL - List Dashboards](https://api.newrelic.com/graphiql?#query=%7B%0A%20%20actor%20%7B%0A%20%20%20%20entitySearch%28queryBuilder%3A%20%7Btype%3A%20DASHBOARD%2C%20name%3A%20%22Terraform%22%7D%2C%20options%3A%20%7B%7D%29%20%7B%0A%20%20%20%20%20%20results%20%7B%0A%20%20%20%20%20%20%20%20entities%20%7B%0A%20%20%20%20%20%20%20%20%20%20...%20on%20DashboardEntityOutline%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20guid%0A%20%20%20%20%20%20%20%20%20%20%20%20name%0A%20%20%20%20%20%20%20%20%20%20%20%20accountId%0A%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%0A%20%20%7D%0A%7D%0A)

## License

This software is released under the [MIT License](https://gitlab.com/potyl/newrelic-dashboard/-/blob/main/LICENSE.txt).
