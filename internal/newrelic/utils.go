package newrelic

import (
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"
	"unicode"
	"unicode/utf8"

	"github.com/newrelic/newrelic-client-go/newrelic"
)

func NewRelicClient() *newrelic.NewRelic {
	apiKey := os.Getenv("NEW_RELIC_API_KEY")
	config := newrelic.ConfigPersonalAPIKey(apiKey)
	client, err := newrelic.New(config)
	if err != nil {
		log.Fatal("Failed to create the newrelic client: ", err)
	}
	return client
}

type AccountIdCorrectorType = func(int) int

func AccountIdCorrector(accountId int) AccountIdCorrectorType {
	return func(gotAccountId int) int {
		if gotAccountId == accountId {
			return 0
		}

		return gotAccountId
	}
}

func CleanDashboardAccountId(dashboard *JsonDashboard, accountIdCorrector AccountIdCorrectorType) {
	// If the accountId matches the default accountId we assign it 0 (zero value)
	// This will have for effect to remove the accountId from final output
	for pageIdx := range dashboard.Pages {
		page := &dashboard.Pages[pageIdx]
		for widgetIdx := range page.Widgets {
			widget := &page.Widgets[widgetIdx]
			for nrqlQueryIdx := range widget.RawConfiguration.NrqlQueries {
				nrqlQuery := &widget.RawConfiguration.NrqlQueries[nrqlQueryIdx]
				nrqlQuery.AccountId = accountIdCorrector(nrqlQuery.AccountId)
			}
		}
	}
}

var indentRegexp = regexp.MustCompile(`^(\s*)`)

func FixIndentation(value string, indent string) string {
	// Find what indent the first line is using, this will be the baseline for indenting the other lines
	originalIndent := indentRegexp.FindString(value)

	// Match all lines that start with the original indent
	re := `(?m)^` + originalIndent

	// Fix the indent for all lines
	return regexp.MustCompile(re).ReplaceAllString(value, indent)
}

func widgetName(name string) string {
	name = strings.Split(name, ".")[1]
	name = strings.ReplaceAll(name, "-", "_")
	return "widget_" + name
}

func escapeQuotedString(input string) string {
	if len(input) == 0 {
		return input
	}

	buffer := make([]byte, 0, len(input))
	for i, r := range input {
		switch r {
		case '\n':
			buffer = append(buffer, '\\', 'n')
		case '\r':
			buffer = append(buffer, '\\', 'r')
		case '\t':
			buffer = append(buffer, '\\', 't')
		case '"':
			buffer = append(buffer, '\\', '"')
		case '\\':
			buffer = append(buffer, '\\', '\\')
		case '$', '%':
			buffer = appendRune(buffer, r)
			remain := input[i+1:]
			if len(remain) > 0 && remain[0] == '{' {
				// Double up our template introducer symbol to escape it.
				buffer = appendRune(buffer, r)
			}
		default:
			if !unicode.IsPrint(r) {
				var formatted string
				if r < 65536 {
					formatted = fmt.Sprintf("\\u%04x", r)
				} else {
					formatted = fmt.Sprintf("\\U%08x", r)
				}
				buffer = append(buffer, formatted...)
			} else {
				buffer = appendRune(buffer, r)
			}
		}
	}

	return string(buffer)
}

func appendRune(buffer []byte, r rune) []byte {
	length := utf8.RuneLen(r)
	for i := 0; i < length; i++ {
		buffer = append(buffer, 0) // make room at the end of our buffer
	}
	char := buffer[len(buffer)-length:]
	utf8.EncodeRune(char, r)
	return buffer
}
