package newrelic

import (
	_ "embed"
	"encoding/json"
	"log"
	"testing"
)

//go:embed fixtures/dashboard.json
var jsonDashboardBytes []byte

//go:embed fixtures/dashboard.tf
var expectedTerraformOutput string

func TestToTerraform(t *testing.T) {
	var dashboard JsonDashboard
	if err := json.Unmarshal(jsonDashboardBytes, &dashboard); err != nil {
		log.Fatal("Failed to parse dashboard: ", err)
	}

	CleanDashboardAccountId(&dashboard, AccountIdCorrector(1234567))
	got := ToTerraform(dashboard, "testing")
	expected := expectedTerraformOutput

	if got != expected {
		t.Errorf("ToTerraform test failed\ngot:      %v\nexpected: %v", got, expected)
	}
}
