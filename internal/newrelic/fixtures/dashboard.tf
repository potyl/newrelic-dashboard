resource "newrelic_one_dashboard" "testing" {
  name        = "Test"
  permissions = "public_read_write"

  page {
    name = "My Billboards"

    widget_billboard {
      title = "Transactions Overview"

      row    = 2
      column = 6
      width  = 7
      height = 3

      warning  = 0.500000
      critical = 3.500000

      nrql_query {
        query = "SELECT count(*) FROM Transactions"
      }
    }

    widget_billboard {
      title = "Errors Overview"

      row    = 5
      column = 6
      width  = 7
      height = 3

      nrql_query {
        query      = "SELECT count(*) FROM Errors"
        account_id = 2345678
      }
    }
  }

  page {
    name = "My Bullets"

    widget_bullet {
      title = "Transactions Overview"

      row    = 2
      column = 6
      width  = 7
      height = 3

      limit = 10.500000

      nrql_query {
        query = "SELECT uniqueCount(session) FROM PageView SINCE 1 day ago"
      }
    }

    widget_bullet {
      title = "Errors Overview"

      row    = 5
      column = 6
      width  = 7
      height = 3

      limit = 10.000000

      nrql_query {
        query      = "SELECT count(*) FROM Errors"
        account_id = 2345678
      }
    }
  }

  page {
    name = "My Markdown"

    widget_markdown {
      title = ""

      row    = 1
      column = 1
      width  = 12
      height = 1

      text = "## This is some markdown"
    }
  }

  page {
    name = "My Pies"

    widget_pie {
      title = "Transactions Overview"

      row    = 2
      column = 6
      width  = 7
      height = 3

      nrql_query {
        query = "SELECT uniqueCount(session) FROM PageView FACET appName SINCE 1 day ago"
      }

      linked_entity_guids = [
        "FooBar",
      ]
      filter_current_dashboard = true
    }

    widget_pie {
      title = "Errors Overview"

      row    = 5
      column = 6
      width  = 7
      height = 3

      nrql_query {
        query      = "SELECT count(*) FROM Errors FACET appName"
        account_id = 2345678
      }

      linked_entity_guids = [
        "BarFoo",
      ]
    }
  }

  page {
    name = "My Tables"

    widget_table {
      title = "Transactions Overview"

      row    = 2
      column = 6
      width  = 7
      height = 3

      nrql_query {
        query = "SELECT count(*) FROM Transactions"
      }
    }

    widget_table {
      title = "Errors Overview"

      row    = 5
      column = 6
      width  = 7
      height = 3

      nrql_query {
        query      = "SELECT count(*) FROM Errors"
        account_id = 2345678
      }
    }
  }
}
