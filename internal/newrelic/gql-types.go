package newrelic

type GraphQLQueryResp struct {
	Actor GraphQLActor
}

type GraphQLActor struct {
	EntitySearch GraphQLEntitySearch
}

type GraphQLEntitySearch struct {
	Results GraphQLResults
}

type GraphQLResults struct {
	Dashboards []GraphQLDashboard `json:"entities"`
}

type GraphQLDashboard struct {
	AccountId           int
	Guid                string
	Name                string
	DashboardParentGuid string
}

type GraphQLExportDashboardResponse struct {
	Actor GraphQLExportDashboardActor
}

type GraphQLExportDashboardActor struct {
	Dashboard JsonDashboard `json:"entity"`
}

type GraphQLDashboardUpdateResult struct {
	DashboardUpdate GraphQLDashboardUpdate
}

type GraphQLDashboardUpdate struct {
	Errors []GraphQLError
}

type GraphQLError struct {
	Description string
	Type        string
}

type GraphQLDashboardCreateResult struct {
	DashboardCreate GraphQLDashboardCreate
}

type GraphQLDashboardCreate struct {
	Errors []GraphQLError
}

type GraphQLDashboardDeleteResult struct {
	DashboardDelete GraphQLDashboardDelete
}
type GraphQLDashboardDelete struct {
	Status string
	Errors []GraphQLError
}

type GraphQLNrqlResp struct {
	Actor GraphQLNrqlActor
}

type GraphQLNrqlActor struct {
	Account GraphQLNrqlAccount
}

type GraphQLNrqlAccount struct {
	Nrql GraphQLNrqlNrql
}

type GraphQLNrqlNrql struct {
	Results []dict
}
