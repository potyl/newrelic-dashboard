package newrelic

import (
	"reflect"
	"testing"
)

func TestHeredoc(t *testing.T) {
	tests := []struct {
		delimiter string
		value     string
		expected  string
	}{
		{"FOO", "", ""},
		{"", "", ""},
		{"", "hello world", "hello world"},
		{"END", "hello world", "hello world"},
	}

	for _, test := range tests {
		got := heredoc(test.delimiter, test.value)
		if got != test.expected {
			t.Errorf("heredoc() test failed, got: %v, expected: %v.", got, test.expected)
		}
	}

	got := heredoc("END", "Hello\nwWorld")
	_, ok := got.(heredocLiteral)
	if !ok {
		t.Error("Assertion error")
	}
}

func TestHeredocLiteralToValue(t *testing.T) {
	tests := []struct {
		delimiter string
		value     string
		indent    syntaxIndent
		expected  string
	}{
		{
			"END",
			"hello\nworld",
			newSyntaxIndent(0),
			`<<-END
  hello
  world
END`,
		},
		{
			"END",
			"hello\nworld",
			newSyntaxIndent(1),
			`<<-END
    hello
    world
  END`,
		},
	}

	for _, test := range tests {
		heredoc := heredocLiteral{test.delimiter, test.value}
		got := heredoc.toValue(test.indent)
		if got != test.expected {
			t.Errorf("heredocLiteral.toValue() test failed, got: %v, expected: %v.", got, test.expected)
		}
	}
}

func TestNewSyntaxIndent(t *testing.T) {
	tests := []struct {
		level    int
		expected syntaxIndent
	}{
		{0, syntaxIndent{}},
		{1, syntaxIndent{1, "  "}},
		{2, syntaxIndent{2, "    "}},
	}

	for _, test := range tests {
		got := newSyntaxIndent(test.level)
		if got != test.expected {
			t.Errorf("newSyntaxIndent(%v) test failed, got: %v, expected: %v.", test.level, got, test.expected)
		}
	}
}

func TestNewSyntaxIndentIncrement(t *testing.T) {
	tests := []struct {
		level     int
		nextLevel int
	}{
		{0, 1},
		{1, 2},
		{2, 3},
	}

	for _, test := range tests {
		got := newSyntaxIndent(test.level).increment()
		expected := newSyntaxIndent(test.nextLevel)
		if got != expected {
			t.Errorf("newSyntaxIndent(%v).increment() test failed, got: %v, expected: %v.", test.level, got, expected)
		}
	}
}

func TestIsZeroValue(t *testing.T) {
	tests := []struct {
		value    any
		expected bool
	}{
		{0, true},
		{1, false},
		{"", true},
		{" ", false},
		{"\n", false},
		{"0", false},
		{"1", false},
		{"false", false},
		{"true", false},
		{true, false},
		{false, true},
		{nil, true},
		{heredoc("END", ""), true},
		{heredoc("END", "0"), false},
		{[]any{}, true},
	}

	for _, test := range tests {
		got := isZeroValue(test.value)
		if got != test.expected {
			t.Errorf("isZeroValue(%v) test failed, got: %v, expected: %v.", test.value, got, test.expected)
		}
	}
}

func TestToValue(t *testing.T) {
	tests := []struct {
		value    any
		level    int
		expected string
	}{
		{0, 0, "0"},
		{1, 0, "1"},
		{"", 0, `""`},
		{" ", 0, `" "`},
		{"\n", 0, `"\n"`},
		{"0", 0, `"0"`},
		{"1", 0, `"1"`},
		{"false", 0, `"false"`},
		{"true", 0, `"true"`},
		{true, 0, "true"},
		{false, 0, "false"},
		// {nil, 0, "nil"},
		{heredoc("END", ""), 0, `""`},
		{heredoc("END", "0"), 0, `"0"`},
		{
			heredoc("END", "hello\nworld"),
			0,
			`<<-END
  hello
  world
END`,
		},
		{
			heredoc("END", "hello\nworld"),
			1,
			`<<-END
    hello
    world
  END`,
		},
		{[0]any{}, 0, "[]"},
		{[0]any{}, 2, "[]"},
		{[2]int{100, 200}, 0, "[\n  100,\n  200,\n]"},
		{[2]int{100, 200}, 2, "[\n      100,\n      200,\n    ]"},
	}

	for _, test := range tests {
		indent := newSyntaxIndent(test.level)
		value := reflect.ValueOf(test.value)
		got := toValue(value, indent)
		if got != test.expected {
			t.Errorf("toValue(%v) test failed, got: %v, expected: %v.", test.value, got, test.expected)
		}
	}
}
