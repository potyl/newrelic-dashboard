package newrelic

import (
	"github.com/newrelic/newrelic-client-go/newrelic"
)

type dict = map[string]any

func CreateDashboard(client *newrelic.NewRelic, accountId int, dashboard JsonDashboard) (GraphQLDashboardCreateResult, error) {
	query := `
		mutation create($accountId: Int!, $dashboard: DashboardInput!) {
			dashboardCreate(accountId: $accountId, dashboard: $dashboard) {
				errors {
					description
					type
				}
			}
		}
	`
	variables := dict{
		"accountId": accountId,
		"dashboard": dashboard,
	}

	var response GraphQLDashboardCreateResult
	err := client.NerdGraph.QueryWithResponse(query, variables, &response)
	return response, err
}

func UploadDashboard(client *newrelic.NewRelic, guid string, dashboard JsonDashboard) (GraphQLDashboardUpdateResult, error) {
	var response GraphQLDashboardUpdateResult

	// https://docs.newrelic.com/docs/apis/nerdgraph/examples/export-import-dashboards-using-api/
	query := `
		mutation update($guid: EntityGuid!, $dashboard: DashboardInput!) {
			dashboardUpdate(guid: $guid, dashboard: $dashboard) {
				errors {
					description
					type
				}
			}
		}
	`

	variables := dict{
		"guid":      guid,
		"dashboard": dashboard,
	}

	err := client.NerdGraph.QueryWithResponse(query, variables, &response)
	return response, err
}

func DeleteDashboard(client *newrelic.NewRelic, guid string) (GraphQLDashboardDeleteResult, error) {
	query := `
		mutation delete($guid: EntityGuid!) {
			dashboardDelete(guid: $guid) {
				status
				errors {
					description
					type
				}
			}
		}
	`

	variables := dict{
		"guid": guid,
	}

	var response GraphQLDashboardDeleteResult
	err := client.NerdGraph.QueryWithResponse(query, variables, &response)
	return response, err
}

func ListDashboards(client *newrelic.NewRelic, search string) (GraphQLQueryResp, error) {
	query := `
		query($search: String!) {
			actor {
				entitySearch(queryBuilder: {type: DASHBOARD, name: $search}, options: {}) {
					results {
						entities {
							... on DashboardEntityOutline {
								guid
								name
								accountId
								dashboardParentGuid
							}
						}
					}
				}
			}
		}
	`

	variables := dict{
		"search": search,
	}

	var response GraphQLQueryResp
	err := client.NerdGraph.QueryWithResponse(query, variables, &response)
	return response, err
}

func ExportDashboard(client *newrelic.NewRelic, guid string) (JsonDashboard, error) {
	query := `
		query($guid: string!) {
			actor {
				entity(guid: $guid) {
					... on DashboardEntity {
						name
						description
						permissions
						pages {
							name
							description
							widgets {
								visualization {
									id
								}
								title
								layout {
									row
									width
									height
									column
								}
								linkedEntities {
									guid
								}
								rawConfiguration
							}
						}
					}
				}
			}
		}
	`

	variables := dict{
		"guid": guid,
	}

	var response GraphQLExportDashboardResponse
	err := client.NerdGraph.QueryWithResponse(query, variables, &response)
	if err != nil {
		return JsonDashboard{}, err
	}

	dashboard := response.Actor.Dashboard

	return dashboard, nil
}

func ExportDashboardRaw(client *newrelic.NewRelic, guid string) (dict, error) {
	query := `
		query($guid: string!) {
			actor {
				entity(guid: $guid) {
					... on DashboardEntity {
						name
						description
						permissions
						pages {
							name
							description
							widgets {
								title
								layout {
									row
									width
									height
									column
								}
								linkedEntities {
									guid
								}
								visualization {
									id
								}
								rawConfiguration
							}
						}
					}
				}
			}
		}
	`

	variables := dict{
		"guid": guid,
	}

	var response dict
	err := client.NerdGraph.QueryWithResponse(query, variables, &response)
	if err != nil {
		return dict{}, err
	}

	dashboard := response["actor"].(dict)["entity"].(dict)
	return dashboard, nil
}

func ExecuteNrql(client *newrelic.NewRelic, accountId int, nrql string) (*GraphQLNrqlResp, error) {
	query := `
		query($accountId: Int!, $query: Nrql!) {
			actor {
				account(id: $accountId) {
					nrql(query: $query) {
						results
					}
				}
			}
		}
	`

	variables := dict{
		"accountId": accountId,
		"query":     nrql,
	}

	var response GraphQLNrqlResp
	err := client.NerdGraph.QueryWithResponse(query, variables, &response)
	return &response, err
}
