package newrelic

type JsonDashboard struct {
	Name        string     `json:"name,omitempty"`
	Description string     `json:"description,omitempty"`
	Permissions string     `json:"permissions,omitempty"`
	Pages       []JsonPage `json:"pages,omitempty"`
}

type JsonPage struct {
	Name        string       `json:"name,omitempty"`
	Description string       `json:"description,omitempty"`
	Widgets     []JsonWidget `json:"widgets,omitempty"`
}

type JsonWidget struct {
	Title                  string                 `json:"title,omitempty"`
	Layout                 JsonLayout             `json:"layout"`
	Visualization          JsonVisualization      `json:"visualization"`
	RawConfiguration       JsonRawConfiguration   `json:"rawConfiguration"`
	LinkedEntityGuids      []JsonLinkedEntityGuid `json:"linkedEntityGuids,omitempty"`
	FilterCurrentDashboard bool                   `json:"filterCurrentDashboard,omitempty"`
}

type JsonLayout struct {
	Column int `json:"column,omitempty"`
	Row    int `json:"row,omitempty"`
	Width  int `json:"width,omitempty"`
	Height int `json:"height,omitempty"`
}

type JsonVisualization struct {
	Id string `json:"id,omitempty"`
}

type JsonLinkedEntityGuid struct {
	Guid string `json:"guid,omitempty"`
}

type JsonRawConfiguration struct {
	NrqlQueries []JsonNrqlQuery `json:"nrqlQueries,omitempty"`
	Text        string          `json:"text,omitempty"`
	Thresholds  []JsonThreshold `json:"thresholds,omitempty"`
	Limit       float64         `json:"limit,omitempty"`
}

type JsonThreshold struct {
	AlertSeverity string  `json:"alertSeverity"`
	Value         float64 `json:"value"`
}

type JsonNrqlQuery struct {
	AccountId int    `json:"accountId,omitempty"`
	Query     string `json:"query,omitempty"`
}
