package newrelic

import (
	"encoding/json"
	"log"
	"strings"

	"github.com/hashicorp/hcl/v2/hclwrite"
)

func ToTerraform(dashboard JsonDashboard, name string) string {
	writer := newTerraformWriter()

	if name == "" {
		name = "default"
	}

	writer.writeResource(
		"newrelic_one_dashboard",
		name,
		func(writer terraformWriter) {
			writer.writeProperty("name", dashboard.Name)
			writer.writePropertyNonZero("description", dashboard.Description)
			writer.writePropertyNonZero("permissions", strings.ToLower(dashboard.Permissions))

			for _, page := range dashboard.Pages {
				writer.writeLn()
				page.write(writer)
			}
		})

	output := writer.String()
	bytes := hclwrite.Format([]byte(output))

	return string(bytes)
}

func ToJson(value any) string {
	bytes, err := json.MarshalIndent(value, "", "  ")
	if err != nil {
		log.Fatal("Failed to serialize to json ", err)
	}

	return string(bytes) + "\n"
}

func (page JsonPage) write(writer terraformWriter) {
	writer.writeBlock("page", func(writer terraformWriter) {
		writer.writeProperty("name", page.Name)
		writer.writePropertyNonZero("description", page.Description)

		for _, widget := range page.Widgets {
			writer.writeLn()
			widget.write((writer))
		}
	})
}

func (widget JsonWidget) write(writer terraformWriter) {
	// Widgets that support linked_entity_guids
	allowsGuids := map[string]bool{
		"widget_bar":   true,
		"widget_pie":   true,
		"widget_table": true,
	}

	widgetName := widgetName(widget.Visualization.Id)
	writer.writeBlock(widgetName, func(writer terraformWriter) {
		writer.writeProperty("title", widget.Title)
		writer.writeLn()

		writer.writeProperty("row", widget.Layout.Row)
		writer.writeProperty("column", widget.Layout.Column)
		writer.writePropertyNonZero("width", widget.Layout.Width)
		writer.writePropertyNonZero("height", widget.Layout.Height)
		writer.writeLn()

		if widgetName == "widget_billboard" {
			written := false
			for _, threshold := range widget.RawConfiguration.Thresholds {
				severity := strings.ToLower(threshold.AlertSeverity)
				written = writer.writePropertyNonZero(severity, threshold.Value) || written
			}
			if written {
				writer.writeLn()
			}
		} else if widgetName == "widget_bullet" {
			writer.writeProperty("limit", widget.RawConfiguration.Limit)
			writer.writeLn()
		}

		if widgetName == "widget_markdown" {
			writer.writeProperty("text", heredoc("MARKDOWN", widget.RawConfiguration.Text))
		} else {
			for _, nrqlQuery := range widget.RawConfiguration.NrqlQueries {
				nrqlQuery.write((writer))
			}
		}

		if len(widget.LinkedEntityGuids) > 0 && allowsGuids[widgetName] {
			writer.writeLn()
			writer.writeProperty("linked_entity_guids", widget.LinkedEntityGuids)
			writer.writePropertyNonZero("filter_current_dashboard", widget.FilterCurrentDashboard)
		}
	})
}

func (nrqlQuery JsonNrqlQuery) write(writer terraformWriter) {
	writer.writeBlock("nrql_query", func(writer terraformWriter) {
		writer.writeProperty("query", heredoc("NRQL", nrqlQuery.Query))
		writer.writePropertyNonZero("account_id", nrqlQuery.AccountId)
	})
}
